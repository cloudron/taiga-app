#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test SSO', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 100000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const PROJECT_NAME = 'testproject';
    const PROJECT_DESCRIPTION = 'testdescription';
    const USER_STORY_SUBJECT = 'someteststory';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login() {
        browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn);
        await browser.executeScript('localStorage.clear();');
        await browser.executeScript('sessionStorage.clear();');
        await browser.get('https://' + app.fqdn + '/login?next=%252Fprofile');
        await waitForElement(By.name('username'));
        await browser.findElement(By.name('username')).sendKeys(process.env.USERNAME);
        await browser.findElement(By.name('password')).sendKeys(process.env.PASSWORD);
        await browser.findElement(By.xpath('//button[@title="Login"]')).click();
        await waitForElement(By.xpath('//h4[text()="Your profile"]'));
    }

    async function signUp() {
        browser.manage().deleteAllCookies();

        await browser.get(`https://${app.fqdn}`);
        await browser.executeScript('localStorage.clear();');
        await browser.executeScript('sessionStorage.clear();');
        await browser.get(`https://${app.fqdn}/register`);
        await waitForElement(By.name('username'));
        await browser.findElement(By.name('username')).sendKeys(process.env.USERNAME);
        await browser.findElement(By.name('full_name')).sendKeys(process.env.USERNAME + ' Name');
        await browser.findElement(By.name('email')).sendKeys('test@cloudron.io');
        await browser.findElement(By.name('password')).sendKeys(process.env.PASSWORD);
        await browser.findElement(By.xpath('//button[@title="Sign up"]')).click();
        await waitForElement(By.xpath('//h1[text()="Projects Dashboard"]'));
    }

    async function adminLogin() {
        await browser.get('https://' + app.fqdn + '/admin/login/');
        await browser.findElement(By.name('username')).sendKeys('admin');
        await browser.findElement(By.name('password')).sendKeys('123123');
        await browser.findElement(By.xpath('//input[@value="Log in"]')).click();
        await waitForElement(By.xpath('//h1[text()="Site administration"]'));
        await browser.findElement(By.xpath('//a[text()="Log out"]')).click();
        await browser.sleep(2000);
    }

    async function userStoryExists() {
        await browser.get('https://' + app.fqdn + '/project/' + process.env.USERNAME + '-' + PROJECT_NAME + '/us/1');
        await waitForElement(By.xpath('//span[text()="' + USER_STORY_SUBJECT + '"]'));
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function createProject() {
        await browser.get('https://' + app.fqdn + '/project/new/scrum');
        await waitForElement(By.name('project-name'));
        await browser.findElement(By.name('project-name')).sendKeys(PROJECT_NAME);
        await browser.sleep(1000);    // it needs some time to change focus!!
        await browser.findElement(By.xpath('//textarea[@ng-model="vm.projectForm.description"]')).sendKeys(PROJECT_DESCRIPTION);
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//button[text()="Create Project"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[text()[contains(., "' + PROJECT_NAME + '")]]')), TEST_TIMEOUT);
    }

    async function createUserStory() {
        await browser.get('https://' + app.fqdn + '/project/' + process.env.USERNAME + '-' + PROJECT_NAME + '/backlog');
        await browser.sleep(10000);
        await waitForElement(By.xpath('//button[@title="Create a new user story"]'));
        await browser.findElement(By.xpath('//button[@title="Create a new user story"]')).click();
        await waitForElement(By.name('subject'));
        await browser.findElement(By.name('subject')).sendKeys(USER_STORY_SUBJECT);
        await waitForElement(By.id('submitButton'));
        await browser.findElement(By.id('submitButton')).click();
        await waitForElement(By.xpath('//span[text()[contains(., "' + USER_STORY_SUBJECT + '")]]'));
    }

    async function deleteProject() {
        await browser.get('https://' + app.fqdn + '/project/' + process.env.USERNAME + '-' + PROJECT_NAME + '/admin/project-profile/details');
        await waitForElement(By.className('delete-project'));
        await browser.findElement(By.className('delete-project')).click();
        await waitForElement(By.xpath('//span[@translate="LIGHTBOX.DELETE_PROJECT.CONFIRM"]'));
        await browser.findElement(By.xpath('//span[@translate="LIGHTBOX.DELETE_PROJECT.CONFIRM"]')).click();
        // give some time to redirect
        await browser.sleep(5000);
    }

    // xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    // it('install app with SSO', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    // it('can get app information', getAppInfo);
    // it('can admin login', adminLogin);
    // it('can login', login);
    // it('can create project', createProject);
    // it('can create user story', createUserStory);
    // it('user story exists', userStoryExists);

    // it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    // it('user story is still present', userStoryExists);

    // it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    // it('restore app', function () {
    //     const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
    //     execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    //     execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    //     getAppInfo();
    //     execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    // });

    // it('user story is still present', userStoryExists);

    // it('move to different location', async function () {
    //     await browser.get('about:blank');
    //     execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    // });

    // it('can get app information', getAppInfo);

    // it('can admin login', adminLogin);
    // // origin change requires new login
    // it('can login', login);
    // it('user story is still present', userStoryExists);

    // it('can delete project', deleteProject);

    // it('uninstall app', async function () {
    //     await browser.get('about:blank');
    //     execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    // });

    // it('install app without SSO', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    // it('can get app information', getAppInfo);
    // it('can admin login', adminLogin);
    // it('can sign up', signUp);
    // it('can create project', createProject);
    // it('can create user story', createUserStory);
    // it('user story exists', userStoryExists);

    // it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    // it('user story is still present', userStoryExists);
    // it('uninstall app', async function () {
    //     await browser.get('about:blank');
    //     execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    // });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id io.taiga.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can login', login);
    it('can create project', createProject);
    it('can create user story', createUserStory);
    it('user story exists', userStoryExists);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can admin login', adminLogin);
    it('can login', login);
    it('user story exists', userStoryExists);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
