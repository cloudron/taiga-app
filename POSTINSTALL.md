This app is pre-setup with an admin account. This admin account is only for the admin panel, not the regular login!
The initial credentials are:

**Username**: admin<br/>
**Password**: 123123<br/>

Please change the admin password and email immediately.

The admin panel is reachable at `/admin/` (trailing slash is important!)

<nosso>
New users can be created in the registration form of the app. Public registration is enabled by default.
</nosso>

<sso>
**Cloudron users must login to the app before being able to add them to projects in Taiga**.
Otherwise, the app assumes they are external users and sends an invite.
</sso>
