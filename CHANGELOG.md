[0.1.0]
* Initial version

[0.1.1]
* Pin revision to e1fe52639b6d9e87

[0.2.0]
* Use upstream version 1.10.0

[0.2.1]
* Use latest smtp settings

[0.3.0]
* New base image
* Update to upstream 2.1.0

[0.4.0]
* New base image
* Update to upstream 3.0.0
* Enable optional single sign-on

[0.5.0]
* New base image 0.10.0

[0.5.1]
* Do not pre-setup an initial first user
* Fix usage information

[0.6.0]
* Update to upstream 3.1.0
* Enable admin panel
* Fix post install
* Fix webhooks

[0.7.0]
* Start using stable release branches instead of release tags

[1.0.0]
* Update to upstream 61c0653 from 21st of July

[1.1.0]
* Update upstream to 30th Aug 2017
* Scale worker count based on available memory

[1.2.0]
* Update to latest 3.1 stable from Oct 19

[1.3.0]
* Update to latest 3.1 stable from Nov 16

[1.3.1]
* Update to version 3.1.3

[1.3.2]
* Update frontend to 3.1.3

[1.4.0]
* Update to 3.2.2

[1.4.1]
* Update Taiga to 3.2.3

[1.5.0]
* Update Taiga to 3.3.2

[1.5.1]
* Update Taiga to 3.3.3

[1.5.2]
* Update Taiga to 3.3.5

[1.5.3]
* Update Taiga to 3.3.7

[1.5.4]
* Update Taiga to 3.3.8

[1.5.5]
* Update Taiga to 3.3.9

[1.5.6]
* Update Taiga to 3.3.12

[1.5.7]
* Update Taiga to 3.3.13

[1.5.8]
* Update Taiga to 3.3.15

[1.6.0]
* Update Taiga to 3.4.1
* Support attaching issues to a sprint
* Ability to link stories to epics
* Redesign of lightbox
* Due date warnings
* New languages and RTL support

[1.6.1]
* Update Taiga to 3.4.2

[1.6.2]
* Update Taiga to 3.4.4

[1.7.0]
* Update Taiga to 3.4.5
* Use new Cloudron base image

[1.8.0]
* Make conf.json and local.py customizable (see docs on how to)

[2.0.0]
* Update Taiga to 4.0.0
* Custom home section
* Custom fields
* Bulk move unfinished objects in sprint
* Paginate history activity
* Improve notifications area

[2.0.1]
* Update Taiga to 4.0.3

[2.0.2]
* Update Taiga to 4.0.4

[2.1.0]
* Update Taiga to 4.1.1

[2.1.1]
* Update Taiga backend to 4.1.1

[2.2.0]
* Update Taiga to 4.2.0

[2.2.1]
* Update Taiga to 4.2.1

[2.2.2]
* Update Taiga to 4.2.3

[2.2.3]
* Update Taiga to 4.2.4

[2.2.4]
* Update Taiga to 4.2.5

[2.2.5]
* Update Taiga to 4.2.6

[2.3.0]
* Update Taiga to 4.2.7

[2.3.1]
* Update Taiga to 4.2.10

[2.3.2]
* Update Taiga to 4.2.11

[2.3.3]
* Update Taiga to 4.2.13

[2.3.4]
* Update Taiga to 4.2.14

[2.4.0]
* Update Taiga to 5.0.0

[2.4.1]
* Update Taiga to 5.0.2

[2.4.2]
* Update Taiga to 5.0.3

[2.4.3]
* Update Taiga frontend to 5.0.4

[2.4.4]
* Update Taiga to 5.0.5

[2.4.5]
* Update Taiga to 5.0.6

[2.4.6]
* Update Taiga to 5.0.7

[2.4.7]
* Update Taiga to 5.0.8

[2.4.8]
* Update Taiga to 5.0.9

[2.4.9]
* Update Taiga to 5.0.10

[2.5.0]
* Update Taiga to 5.0.11
* Update base image to 2.0.0

[2.5.1]
* Update Taiga to 5.0.12

[2.5.2]
* Update Taiga to 5.0.13

[2.5.3]
* Update Taiga to 5.0.14

[2.5.4]
* Update Taiga to 5.0.15

[2.6.0]
* Update Taiga to 5.5.1

[2.6.1]
* Update Taiga to 5.5.2

[2.6.2]
* Update Taiga to 5.5.3

[2.6.3]
* Update Taiga to 5.5.4

[2.6.4]
* Update Taiga to 5.5.5

[2.6.5]
* Update slack plugin to 5.5.1

[2.6.6]
* Update Taiga to 5.5.6

[2.6.7]
* Update Taiga to 5.5.7

[2.6.8]
* Update Taiga frontend to 5.5.8

[2.6.9]
* Update Taiga to 5.5.9

[2.6.10]
* Update Taiga UI to 5.5.10

[2.7.0]
* Update Taiga to 6.0.3

[2.7.1]
* Update Taiga to 6.0.4

[2.7.2]
* Update Taiga to 6.0.5

[2.7.3]
* Update Taiga to 6.0.6

[2.7.4]
* Update Taiga to 6.0.7

[2.7.5]
* Update Taiga to 6.0.8

[2.7.6]
* Update Taiga to 6.0.9

[2.8.0]
* Update Taiga to 6.1.1

[2.9.0]
* Update Taiga to 6.2.2

[2.10.0]
* Update Taiga to 6.3.3

[2.11.0]
* Update Taiga to 6.4.2

[2.11.1]
* Update Taiga to 6.4.3

[2.11.2]
* Update base image to 3.2.0

[2.11.3]
* Update Taiga Front to 6.5.0

[2.11.4]
* Update Taiga Back to 6.5.0

[2.11.5]
* Update Taiga Front and Back to 6.5.1

[2.11.6]
* Update Taiga Back to 6.5.2
* Update Taiga Front to 6.5.2

[2.12.0]
* Update Taiga Back to 6.6.0
* Update Taiga Frontto 6.6.0
* Set maxBoxVersion to 7.3 . Taiga does not work with PostgreSQL 14 (https://github.com/kaleidos-ventures/taiga-back/issues/122)

[2.12.1]
* Update Taiga Back to 6.6.1

[2.12.2]
* Update Taiga Back to 6.6.2

[2.13.0]
* Update Taiga to 6.7.0

[2.13.1]
* Update Taiga to 6.7.1

[2.14.0]
* Update base image to 4.2.0

[2.14.1]
* Update Taiga to 6.7.2

[2.14.2]
* Update Taiga Front to 6.7.4
* GitHub no longer supports subversion, change plugins setup system

[2.14.3]
* Update Taiga Front to 6.7.6
* Update Taiga Front to 6.7.3

[2.14.4]
* Update Taiga Front to 6.7.7

[2.15.0]
* Update Taiga Front to 6.8.0
* Update Taiga Front to 6.8.0

[2.15.1]
* Update Taiga Front to 6.8.1
* Update Taiga Front to 6.8.1

[2.15.2]
* Update taiga to 6.8.2

