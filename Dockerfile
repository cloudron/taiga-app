FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/taiga-back /app/code/taiga-front-dist /app/pkg
WORKDIR /app/code

RUN apt-get update && apt-get install -y binutils-doc flex bison tmux virtualenvwrapper && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

## sometimes for minor releases the versions differ

# renovate: datasource=github-tags depName=kaleidos-ventures/taiga-back versioning=semver
ARG TAIGA_BACK_VERSION=6.8.2

# renovate: datasource=github-tags depName=kaleidos-ventures/taiga-front-dist versioning=semver
ARG TAIGA_FRONT_VERSION=6.8.2

RUN curl -L https://github.com/kaleidos-ventures/taiga-back/archive/${TAIGA_BACK_VERSION}.tar.gz | tar -xz -C /app/code/taiga-back --strip-components 1 -f -
RUN curl -L https://github.com/kaleidos-ventures/taiga-front-dist/archive/${TAIGA_FRONT_VERSION}.tar.gz | tar -xz -C /app/code/taiga-front-dist --strip-components 1 -f -

RUN npm install json

ADD build.sh nginx.conf conf.json conf_ldap.json json-merge.js /app/code/

## install all deps in a python virtual env
RUN /bin/bash /app/code/build.sh

RUN rm -rf /app/code/taiga-back/media && ln -s /app/data/media /app/code/taiga-back/media && \
    rm -rf /var/log/nginx && mkdir /run/nginx && ln -s /run/nginx /var/log/nginx && \
    rm -f /app/code/taiga-front-dist/dist/conf.json && ln -s /app/data/conf.json /app/code/taiga-front-dist/dist/conf.json

COPY local.py /app/code/taiga-back/settings/config.py
COPY start.sh /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
