#!/bin/bash

set -eu -o pipefail

echo "========= Build ========="

echo "setup taiga virtualenv"
cd /app/code
virtualenv -p /usr/bin/python3.10 taiga
source /app/code/taiga/bin/activate

echo "install taiga deps"
cd /app/code/taiga-back
pip install -r requirements.txt

echo "install taiga-contrib-ldap-auth"
pip install taiga-contrib-ldap-auth

echo "install taiga-contrib-slack"
# curl -s https://pypi.org/pypi/taiga-contrib-slack/json | grep -oE '"version":"[[:digit:]]+(\.[[:digit:]]+)?(\.[[:digit:]]+)?"'
readonly taiga_slack_version=5.5.1
pip install taiga-contrib-slack==${taiga_slack_version}

echo "run migration scripts"
cd /app/code/taiga-back
python manage.py compilemessages --exclude eu
python manage.py collectstatic --noinput

## Install frontend of plugins
echo "installing slack plugin"
mkdir -p /app/code/taiga-front-dist/dist/plugins/slack
cd /app/code/taiga-front-dist/dist/plugins/slack
curl https://raw.githubusercontent.com/taigaio/taiga-contrib-slack/${taiga_slack_version}/front/dist/slack.js -o ./slack.js
curl https://raw.githubusercontent.com/taigaio/taiga-contrib-slack/${taiga_slack_version}/front/dist/slack.json -o ./slack.json

