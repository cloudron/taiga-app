#!/bin/bash

# set -eu -o pipefail

# will be included in local.py
if [[ ! -f /app/data/customlocal.py ]]; then
    echo -e "# Place custom local.py settings in this file\n" > /app/data/customlocal.py
fi

# create and merge any user conf.json
if [[ ! -f /app/data/conf.json ]]; then
    echo "{}" > /app/data/conf.json
fi

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "=> Update conf.json with LDAP"
    node /app/code/json-merge.js /app/data/conf.json /app/code/conf_ldap.json
else
    echo "=> Update conf.json"
    node /app/code/json-merge.js /app/data/conf.json /app/code/conf.json
fi

/app/code/node_modules/.bin/json -I -f /app/data/conf.json -e "this.api = '${CLOUDRON_APP_ORIGIN}/api/v1/'"

echo "=> Update nginx.conf"
sed -e "s,##APP_DOMAIN##,${CLOUDRON_APP_DOMAIN}," /app/code/nginx.conf  > /run/nginx.conf

echo "=> Setup taiga virtual env"
source /app/code/taiga/bin/activate

export DJANGO_SETTINGS_MODULE=settings.config

cd /app/code/taiga-back

if [[ ! -d /app/data/media/user ]]; then
    echo "=> New installation create inital project templates"

    echo "=> Run migration scripts"
    mkdir -p /app/data/media/user

    python manage.py migrate --noinput
    python manage.py loaddata initial_user
    python manage.py loaddata initial_project_templates
else
    echo "=> Run migration scripts"
    python manage.py migrate --noinput
fi

echo "=> Make cloudron own /run"
chown -R cloudron:cloudron /run
chown -R cloudron:cloudron /app/data

echo "=> Start nginx"
nginx -c /run/nginx.conf &

echo "=> Start taiga-back"
PATH=/app/code/taiga/bin:$PATH
HOME=/app/code
PYTHONPATH=/app/code/taiga/lib/python3.6/site-packages

cd /app/code/taiga-back

memory_limit=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)
worker_count=$((memory_limit/1024/1024/150)) # 1 worker for 150M
worker_count=$((worker_count > 8 ? 8 : worker_count )) # max of 8
worker_count=$((worker_count < 1 ? 1 : worker_count )) # min of 1

echo "Starting gunicorn with ${worker_count} workers"
exec /usr/local/bin/gosu cloudron:cloudron gunicorn -w ${worker_count} -t 60 --pythonpath=. -b 127.0.0.1:8001 taiga.wsgi
